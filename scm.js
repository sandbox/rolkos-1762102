(function($) {
	Drupal.behaviors.simple_chained_menu_elements_form = {
		attach : function(context, settings) {
			$('#ajax_child_div #child-select').change(function() {
				$(this).parents("form").submit();
			});
		}
	}
})(jQuery);